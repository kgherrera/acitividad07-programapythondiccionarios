'''
Created on 14/09/2021

@author: Herrera
'''
class Alumno:
    
    def __init__(self, nombre, edad, carrera,fecha):
        self.nombre = nombre
        self.edad = edad
        self.carrera = carrera
        self.fecha = fecha
        
    def __str__(self):
        return f"nombre: {self.nombre} edad: {self.edad} carrera: {self.carrera} fecha: {self.fecha}"

opcion = 0
diccionarioAlumnos = []  
for i in range(5):
    print(diccionarioAlumnos[i])

while(opcion != 6):
    print("\nElige una de las siguientes opciones");
    print("1) Llenar lista");
    print("2) Vaciar lista");
    print("3) Mostrar los alumnos por carrera");
    print("4) Calcular Promedio de edades");
    print("5) Mostrar los alumnos que se inscribieron despues de la fecha indicada (10/08/2016)");
    print("6) Salir");
    opcion = int(input("Introduce opcion: "))
    
    if(opcion == 1):
        for i in range(5):
            print("\nIntroduce los siguientes datos: ")
            nombre = input("nombre: ")
            edad = int(input("Edad: "))
            carrera = input("Carrera: ").upper()
            fecha = input("Fecha de inscripcion");
            alumno = Alumno(nombre, edad, carrera, fecha)
            diccionarioAlumnos[len(diccionarioAlumnos) + 1] = alumno 
            
    elif(opcion == 2):
        diccionarioAlumnos.clear()
        print("\n>> Diccionario vaciado correctamente")
    
    elif(opcion == 3):
        if(len(diccionarioAlumnos)>=1):
            carreraSolicitada = input("\nIntroduce carrera de alumnos a mostrar: ").upper()
            print(f"\nAlumnos con la carrera {carreraSolicitada}: ")
            for i in range(len(diccionarioAlumnos)):
                carreraAlumno = diccionarioAlumnos[i].carrera
                if(carreraSolicitada == carreraAlumno):
                    print(diccionarioAlumnos[i])
        
        else:
            print("\n>> Diccionario vacio") 
    elif (opcion == 4):
        if(len(diccionarioAlumnos)>=1):
            suma = 0
            for i in range(len(diccionarioAlumnos)):
                suma+=diccionarioAlumnos[i].edad
            promedio = suma/(len(diccionarioAlumnos))
            print(f"\nPromedio de edades: {promedio}")
        else:
            print("\n>> Diccionario vacio") 
    
    elif (opcion == 5):
        print("\n>> Alumnos inscritos despues del 10/08/2016: ")
        if(len(diccionarioAlumnos)>=1):
            dia = 10
            mes = 8
            year = 2016
            for i in range(len(diccionarioAlumnos)):
                fechaStrings = diccionarioAlumnos[i].fecha.split("/")
                diaAlumno = int(fechaStrings[0])
                mesAlumno = int(fechaStrings[1])
                yearAlumno = int(fechaStrings[2])
                
                
                if (yearAlumno > year):
                    print(diccionarioAlumnos[i])
                    continue
                elif (yearAlumno < year):
                    continue
                if (mesAlumno > mes):
                    print(diccionarioAlumnos[i])
                    continue
                elif (mesAlumno < mes):
                    continue
                
                if (diaAlumno > dia):
                    print(diccionarioAlumnos[i])
                
        else:
            print("\n>> Diccionario vacio") 
    elif (opcion == 6):
        print("Saliendo . . .")
    else: 
        print("\n>> Opcion Incorrecta ")
        
        
    
    

    
    
            